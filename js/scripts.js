$(document).ready(function() {
    $('#mycarousel').carousel({ interval: 2000 });
    $('#carouselButton').click(function() {
     if ($('#carouselButton').children('span').hasClass('fa-pause')) {
         $('#mycarousel').carousel('pause');
         $('#carouselButton').children('span').removeClass('fa-pause');
         $('#carouselButton').children('span').addClass('fa-play');
        }
        else if ($('#carouselButton').children('span').hasClass('fa-play')) {
         $('#mycarousel').carousel('cycle');
         $('#carouselButton').children('span').removeClass('fa-play');
         $('#carouselButton').children('span').addClass('fa-pause');
        }
    });
 });
 $(document).ready(function() {
   $('#LoginButton').click(function() {
       $('#loginModal').modal('show');
   });
   $('#LoginClose1').click(function() {
       $('#loginModal').modal('hide');
   });
   $('#LoginClose2').click(function() {
       $('#loginModal').modal('hide');
   });
   $('#ReserveButtton').click(function() {
       $('#Reservemodal').modal('show');
   });
   $('#ReserveClose1').click(function() {
       $('#Reservemodal').modal('hide');
   });
   $('#ReserveClose2').click(function() {
       $('#Reservemodal').modal('hide');
   });
 });